﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts a <see cref="bool" /> value into a binary representation.
        /// </summary>
        /// <param name="value">The <see cref="bool" /> value you want to convert to binary</param>
        public static byte ToBinary(this bool value)
        {
            return Convert.ToByte(value);
        }
    }
}
