﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts a <see cref="bool" /> value to a &quot;yes/no&quot; string.
        /// </summary>
        /// <param name="value">The <see cref="bool" /> value you want to parse</param>
        /// <param name="yes">The value to return if the result is true</param>
        /// <param name="no">The value to return if the result is false</param>
        public static string ToYesNoString(this bool value, string yes = "yes", string no = "no")
        {
            return value ? yes : no;
        }

        /// <summary>
        /// Converts a <see cref="bool" /> value to a &quot;yes/no&quot; string.
        /// </summary>
        /// <param name="value">The <see cref="bool" /> value you want to parse</param>
        /// <param name="yes">The value to return if the result is true</param>
        /// <param name="no">The value to return if the result is false</param>
        /// <param name="nullValue">The value to return if the <see cref="bool" /> value is null</param>
        public static string ToYesNoString(this bool? value,
            string yes = "yes", string no = "no", string nullValue = null)
        {
            return !value.HasValue
                ? nullValue
                : ToYesNoString(value.Value, yes, no);
        }
    }
}
