﻿using System;
using System.Collections.Generic;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Shuffles the items in selected list
        /// </summary>
        /// <param name="random">A <see cref="Random" /> generator</param>
        /// <param name="list">The list with items</param>
        public static void Shuffle<TElement>(this Random random, IList<TElement> list)
        {
            int itemsCount = list.Count;
            while (itemsCount > 1)
            {
                itemsCount--;
                var k = random.Next(itemsCount + 1);
                var value = list[k];
                list[k] = list[itemsCount];
                list[itemsCount] = value;
            }
        }
    }
}
