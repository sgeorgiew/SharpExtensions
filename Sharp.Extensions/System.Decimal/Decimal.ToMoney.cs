﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="decimal" /> value to money.
        /// </summary>
        /// <param name="value">The <see cref="decimal" /> value to act on</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static decimal ToMoney(this decimal value)
        {
            return Math.Round(value, 2);
        }
    }
}
