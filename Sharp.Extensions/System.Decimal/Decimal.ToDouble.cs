﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="decimal" /> value to his equivalent double-precision floating-point number.
        /// </summary>
        /// <param name="value">The <see cref="decimal" /> value to convert</param>
        public static double ToDouble(this decimal value)
        {
            return decimal.ToDouble(value);
        }
    }
}
