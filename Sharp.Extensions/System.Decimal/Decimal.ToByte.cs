﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="decimal" /> value to his equivalent 8-bit unsigned integer.
        /// </summary>
        /// <param name="value">The <see cref="decimal" /> value to convert</param>
        /// <exception cref="OverflowException"></exception>
        public static byte ToByte(this decimal value)
        {
            return decimal.ToByte(value);
        }
    }
}
