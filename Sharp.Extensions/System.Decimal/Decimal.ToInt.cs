﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="decimal" /> value to his equivalent 32-bit signed integer.
        /// </summary>
        /// <param name="value">The <see cref="decimal" /> value to convert</param>
        /// <exception cref="OverflowException"></exception>
        public static int ToInt32(this decimal value)
        {
            return decimal.ToInt32(value);
        }
    }
}
