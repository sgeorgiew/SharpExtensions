﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> representation of a number to its <see cref="int" /> equivalent.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        public static int ToInt(this string value)
        {
            int.TryParse(value, out int number);
            return number;
        }
    }
}
