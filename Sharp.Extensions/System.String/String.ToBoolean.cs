﻿using System;
using System.Linq;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> to its <see cref="bool" /> equivalent.<para/>
        /// Returns False if the value is: 0, false, f, off, no, n.<para/>
        /// Returns True if the value is: 1, true, t, on, yes, y.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <exception cref="ArgumentException"></exception>
        public static bool ToBoolean(this string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                throw new ArgumentException(nameof(value));

            string val = value.ToLower().Trim();

            string[] booleanStringFalse = { "0", "false", "f", "off", "no", "n" };
            string[] booleanStringTrue = { "1", "true", "t", "on", "yes", "y" };

            if (booleanStringFalse.Contains(val, StringComparer.InvariantCultureIgnoreCase))
                return false;

            if (booleanStringTrue.Contains(val, StringComparer.InvariantCultureIgnoreCase))
                return true;

            return bool.TryParse(val, out bool result);
        }
    }
}
