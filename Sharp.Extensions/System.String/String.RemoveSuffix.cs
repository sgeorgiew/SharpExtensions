﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Removes the end part of the <see cref="string" />, if no match found returns the original <see cref="string" />.
        /// </summary>
        /// <param name="value">The original <see cref="string" /> value</param>
        /// <param name="suffix">The <see cref="string" /> value suffix to remove</param>
        /// <param name="ignoreCase">Should it be case sensitive</param>
        public static string RemoveSuffix(this string value, string suffix, bool ignoreCase = true)
        {
            if (!string.IsNullOrEmpty(value) &&
                (ignoreCase ? value.EndsWithIgnoreCase(suffix) : value.EndsWith(suffix)))
            {
                return value.Substring(0, value.Length - suffix.Length);
            }

            return value;
        }
    }
}
