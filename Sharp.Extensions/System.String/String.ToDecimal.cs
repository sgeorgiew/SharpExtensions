﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> representation of a number to its <see cref="decimal" /> equivalent.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        public static decimal ToDecimal(this string value)
        {
            decimal.TryParse(value, out decimal number);
            return number;
        }
    }
}
