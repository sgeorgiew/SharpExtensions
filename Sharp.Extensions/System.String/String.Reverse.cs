﻿using System;
using System.Text;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Reverses the order of characters in a <see cref="string" />.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to reverse</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static string Reverse(this string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (value.Length <= 1)
                return value;

            var stringBuilder = new StringBuilder(value.Length);
            for (var i = value.Length - 1; i >= 0; i--)
                stringBuilder.Append(value[i]);

            return stringBuilder.ToString();
        }
    }
}
