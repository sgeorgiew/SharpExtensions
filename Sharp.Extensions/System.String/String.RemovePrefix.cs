﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Removes the first part of the <see cref="string" />, if no match found returns the original <see cref="string" />.
        /// </summary>
        /// <param name="value">The original <see cref="string" /> value</param>
        /// <param name="prefix">The <see cref="string" /> value prefix to remove</param>
        /// <param name="ignoreCase">Should it be case sensitive</param>
        public static string RemovePrefix(this string value, string prefix, bool ignoreCase = true)
        {
            if (!string.IsNullOrEmpty(value) &&
                (ignoreCase ? value.StartsWithIgnoreCase(prefix) : value.StartsWith(prefix)))
            {
                return value.Substring(prefix.Length, value.Length - prefix.Length);
            }

            return value;
        }
    }
}
