﻿using System;
using System.Text;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Calculates the amount of bytes occupied by the input <see cref="string" /> encoded as the <see cref="Encoding" /> specified.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <param name="encoding">The <see cref="Encoding" /> to usе</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static int GetByteSize(this string value, Encoding encoding)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (encoding == null)
                throw new ArgumentNullException(nameof(encoding));

            return encoding.GetByteCount(value);
        }
    }
}
