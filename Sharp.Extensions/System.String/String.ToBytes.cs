﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> to its byte[] equivalent.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <exception cref="ArgumentException"></exception>
        public static byte[] ToBytes(this string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                throw new ArgumentException(nameof(value));

            var bytes = new byte[value.Length * sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);

            return bytes;
        }
    }
}
