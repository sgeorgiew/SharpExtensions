﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> to <see cref="Enum" />.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <param name="ignoreCase">Should it be case sensitive</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static T ToEnum<T>(this string value, bool ignoreCase = true)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        /// <summary>
        /// Converts the <see cref="string" /> to <see cref="Enum" /> or returns default value if the <see cref="string" /> is null or empty.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <param name="defaultValue">The default value to return if the <see cref="string" /> is null or empty</param>
        /// <param name="ignoreCase">Should it be case sensitive</param>
        /// <exception cref="ArgumentException"></exception>
        public static T ToEnumOrDefault<T>(this string value, T defaultValue, bool ignoreCase = true)
            where T : struct
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            return Enum.TryParse(value, ignoreCase, out T result)
                ? result
                : defaultValue;
        }
    }
}
