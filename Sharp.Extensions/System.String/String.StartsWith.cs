﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Checks if a <see cref="string" /> starts with another <see cref="string" /> with ignoring the case.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to check</param>
        /// <param name="prefix">The <see cref="string" /> value prefix to use</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static bool StartsWithIgnoreCase(this string value, string prefix)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (prefix == null)
                throw new ArgumentNullException(nameof(prefix));

            if (value.Length < prefix.Length)
                return false;

            return value.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
