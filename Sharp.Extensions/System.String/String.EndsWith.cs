﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Checks if a <see cref="string" /> ends with another <see cref="string" /> with ignoring the case.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to check</param>
        /// <param name="suffix">The <see cref="string" /> value suffix to use</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static bool EndsWithIgnoreCase(this string value, string suffix)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (suffix == null)
                throw new ArgumentNullException(nameof(suffix));

            if (value.Length < suffix.Length)
                return false;

            return value.EndsWith(suffix, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
