﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a randomly selected item from the array.
        /// </summary>
        /// <param name="array">The <see cref="Array" /> with items</param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public static T GetRandom<T>(this T[] array)
        {
            if (array.Length <= 0)
                throw new IndexOutOfRangeException(nameof(array));

            return array[new Random().Next(array.Length)];
        }
    }
}
