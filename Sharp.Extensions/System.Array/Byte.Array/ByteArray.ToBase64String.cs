﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
        /// </summary>
        /// <param name="byteArray">The <see cref="byte" /> array to convert</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static string ToBase64String(this byte[] byteArray)
        {
            return byteArray.ToBase64String(Base64FormattingOptions.None);
        }

        /// <summary>
        /// Converts an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
        /// </summary>
        /// <param name="byteArray">The <see cref="byte" /> array to convert</param>
        /// <param name="options">Specifies whether to insert line breaks</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static string ToBase64String(this byte[] byteArray, Base64FormattingOptions options)
        {
            if (byteArray == null)
                throw new ArgumentNullException(nameof(byteArray));

            return Convert.ToBase64String(byteArray, options);
        }

        /// <summary>
        /// Converts a subset of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
        /// </summary>
        /// <param name="byteArray">The <see cref="byte" /> array to convert</param>
        /// <param name="offset">The offset value</param>
        /// <param name="length">The number of elements to convert</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static string ToBase64String(this byte[] byteArray, int offset, int length)
        {
            return byteArray.ToBase64String(offset, length, Base64FormattingOptions.None);
        }

        /// <summary>
        /// Converts a subset of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
        /// </summary>
        /// <param name="byteArray">The <see cref="byte" /> array to convert</param>
        /// <param name="offset">The offset value</param>
        /// <param name="length">The number of elements to convert</param>
        /// <param name="options">Specifies whether to insert line breaks</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static string ToBase64String(this byte[] byteArray, int offset, int length, Base64FormattingOptions options)
        {
            if (byteArray == null)
                throw new ArgumentNullException(nameof(byteArray));

            return Convert.ToBase64String(byteArray, offset, length, options);
        }
    }
}
