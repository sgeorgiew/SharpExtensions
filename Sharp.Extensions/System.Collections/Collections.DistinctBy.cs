﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns the set of items, made distinct by the selected value.
        /// </summary>
        /// <param name="items">The items collection</param>
        /// <param name="property">A function that selects a value to determine unique results</param>
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }
    }
}
