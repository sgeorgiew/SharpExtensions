﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Adds items from <see cref="IEnumerable" /> to <see cref="ICollection" />.
        /// </summary>
        /// <param name="collection">The <see cref="ICollection" /> items</param>
        /// <param name="itemsToAdd">The <see cref="IEnumerable" /> items to add</param>
        public static void AddRange<TElement>(this ICollection<TElement> collection, IEnumerable<TElement> itemsToAdd)
        {
            foreach (var item in itemsToAdd)
            {
                collection.Add(item);
            }
        }
    }
}
