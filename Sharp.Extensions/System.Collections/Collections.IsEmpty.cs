﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Determines whether the <see cref="IEnumerable" /> is empty.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static bool IsEmpty(this IEnumerable enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            return !enumerable.GetEnumerator().MoveNext();
        }

        /// <summary>
        /// Determines whether the <see cref="IEnumerable" /> is empty.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            return (enumerable.Count() <= 0);
        }
    }
}
